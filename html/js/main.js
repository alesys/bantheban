var input_init_value;
var isMobile = !!navigator.platform && /iPad|iPhone|iPod|Android/.test(navigator.platform);
function init() {
    $("#input").on('keyup change keypress', startDelay);
    $("#input").focus(function(){
        if (this.value==input_init_value){
            this.value = "";
        }
    });
    input_init_value = $("#input").val();
    $("#btn").click(cta);
    // clip
    var clipboard = new Clipboard('#btn', {
        text: function(trigger) {
            return $("#output").val();
        }
    });
    clipboard.on('success', function(e) {
        console.log('success copying');
        open("https://twitter.com/");
    });
    //if(!isMobile)$("#input").focus();
    setTimers();
    onTick(); // tick right at the start
}

var delayID;
function startDelay() {
    if (delayID != false) {
        clearInterval(delayID);
    }
    delayID = setInterval(onTimeOut, 200);
}
function onTimeOut() {
    clearInterval(delayID);
    delayID = false;
    translate();
}

function translate() {
    var text = $("#input").val();
    var url = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20170130T194759Z.a40f58b2a6ef0c0a.1caafc7357d687a3c30be1ac2b434a292421a160&text=" + text + "&lang=en-ar";
    $.get(url, function (result) {
        var text = $("#input").val();
        var str = analyzeString(text, result.text[0]);
        $("#output").val(str);
    });
}

function analyzeString($_original, $_translated){
    console.log($_original, $_translated);
    var words_original = $_original.split(' ');
    var words_translated = $_translated.split(' ');

    for (var i = 0; i < words_translated.length; i++) {
        var word_translated = words_translated[i];
        for (var j = 0; j < words_original.length; j++) {
            var word_original = words_original[j];
            if(word_original == word_translated){
                console.log("word not translated, replacing:", word_original);
                var words_backup = ["سلام","حرية","سويا","أهلا بك","البشر"];
                var word_backup = words_backup[Math.floor(Math.random()*words_backup.length)];
                words_translated[i] = word_backup;
            }
        }
    }

    // decompose

    return words_translated.join(" ");
}

function cta(){
    $.get("php/inc_count.php", function(result){console.log("count added", result)});
}

var timerInterval;
function setTimers (){
    timerInterval = setInterval( onTick, 2000)
}
function onTick (){
    $.get("php/get_count.php", function(result){
        $("#counter_output").html(result);

    });
}

function showCounter(){
    $("#counter").show();
}
$(document).ready(init);